"""define train, infer, eval process"""

import collections
import numpy as np
import os, sys
import time
import tensorflow as tf
from IO.iterator import DsnIterator, DsnSingleChannelIterator
from src.dsn import DsnModel
from src.DSNSingleAttention import DSNSingleAttentionModel
from src.dsnSingleChannel import dsnSingleChannel
from src.dssm import DSSM
from IO.dsn_cache import DsnCache
from IO.dsn_single_channel_cache import DsnSingleCache

import utils.util as util
import utils.metric as metric
from utils.log import logger


# from hparams import *
class TrainModel(collections.namedtuple("TrainModel", ("graph", "model", "iterator"))):
    """define train class, include graph, model, iterator"""
    pass


def create_train_model(model_creator, hparams, train_file, batch_iterator, scope=None):
    graph = tf.Graph()
    with graph.as_default():
        filenames = [train_file]
        src_dataset = tf.contrib.data.TFRecordDataset(filenames)
        # 构建训练数据的迭代器
        batch_input = batch_iterator(src_dataset, hparams)
        # 创建model
        model = model_creator(
            hparams,
            mode=tf.contrib.learn.ModeKeys.TRAIN,
            iterator=batch_input,
            scope=scope)
    # 返回train graph, 含有graph, model, iterator
    return TrainModel(
        graph=graph,
        model=model,
        iterator=batch_input)


class EvalModel(collections.namedtuple("EvalModel", ("graph", "model", "iterator"))):
    """define eval class, include graph, model, iterator"""
    pass


def create_eval_model(model_creator, hparams, eval_file, batch_iterator, scope=None):
    graph = tf.Graph()
    with graph.as_default():
        filenames = [eval_file]
        src_dataset = tf.contrib.data.TFRecordDataset(filenames)
        # 构建训练数据的迭代器
        batch_input = batch_iterator(src_dataset, hparams)
        # 创建model
        model = model_creator(
            hparams,
            mode=tf.contrib.learn.ModeKeys.EVAL,
            iterator=batch_input,
            scope=scope)
    # 返回train graph, 含有graph, model, iterator
    return EvalModel(
        graph=graph,
        model=model,
        iterator=batch_input)


def cache_train_data(hparams, cache_class):
    cache_obj = cache_class()
    if not os.path.exists(util.CACHE_DIR):
        os.mkdir(util.CACHE_DIR)
    if not hparams.train_file is None:
        hparams.train_file_cache = util.convert_cached_name(hparams.train_file, hparams.batch_size)
        print('train_cache:', hparams.train_file_cache)
        if not os.path.isfile(hparams.train_file_cache):
            print('has not cached train file, begin cached...')
            start_time = time.time()
            cache_obj.write_tfrecord(hparams.train_file, hparams.train_file_cache, hparams)


def cache_eval_data(hparams, cache_class):
    cache_obj = cache_class()
    if not os.path.exists(util.CACHE_DIR):
        os.mkdir(util.CACHE_DIR)
    if not hparams.eval_file is None:
        hparams.eval_file_cache = util.convert_cached_name(hparams.eval_file, hparams.batch_size)
        print('eval_cache:', hparams.eval_file_cache)
        if not os.path.isfile(hparams.eval_file_cache):
            print('has not cached eval file, begin cached...')
            start_time = time.time()
            cache_obj.write_tfrecord(hparams.eval_file, hparams.eval_file_cache, hparams)


# run evaluation and get evaluted loss
def run_eval(load_model, load_sess, model_dir, sample_num_file, hparams, flag):
    load_model.model.saver.restore(load_sess, model_dir)
    load_sess.run(load_model.iterator.initializer)
    preds = []
    labels = []
    while True:
        try:
            _, _, step_pred, step_labels = load_model.model.eval(load_sess)
            preds.extend(np.reshape(step_pred, -1))
            labels.extend(np.reshape(step_labels, -1))
        except tf.errors.OutOfRangeError:
            break
    res = metric.cal_metric(labels, preds, hparams, flag)
    return res


def train(hparams, scope=None, target_session=""):
    params = hparams.values()
    for key, val in params.items():
        logger.info(str(key) + ':' + str(val))

    print('load and cache data...')
    if hparams.model_type == 'Dsn':
        model_creator = DsnModel
        batch_input = DsnIterator
        cache_obj = DsnCache
    elif hparams.model_type == 'DSNSingleAttention':
        model_creator = DSNSingleAttentionModel
        batch_input = DsnIterator
        cache_obj = DsnCache
    elif hparams.model_type == 'dsnSingleChannel':
        model_creator = dsnSingleChannel
        batch_input = DsnSingleChannelIterator
        cache_obj = DsnSingleCache
    elif hparams.model_type == 'dssm':
        model_creator = DSSM
        batch_input = DsnSingleChannelIterator
        cache_obj = DsnSingleCache
    else:
        raise ValueError("model type is not defined!")
    cache_train_data(hparams, cache_obj)
    cache_eval_data(hparams, cache_obj)
    # define train,eval,infer graph
    # define train session, eval session, infer session
    train_model = create_train_model(model_creator, hparams, hparams.train_file_cache, batch_input, scope)
    train_sess = tf.Session(target=target_session, graph=train_model.graph)

    eval_model_train = create_eval_model(model_creator, hparams, hparams.train_file_cache, batch_input, scope)
    eval_train_sess = tf.Session(target=target_session, graph=eval_model_train.graph)

    eval_model_eval = create_eval_model(model_creator, hparams, hparams.eval_file_cache, batch_input, scope)
    eval_eval_sess = tf.Session(target=target_session, graph=eval_model_eval.graph)

    train_sess.run(train_model.model.init_op)
    if not hparams.load_model_name is None:
        checkpoint_path = hparams.load_model_name
        try:
            train_model.model.saver.restore(train_sess, checkpoint_path)
        except:
            raise IOError("Failed to find any matching files for {0}".format(checkpoint_path))
        else:
            print('load model', checkpoint_path)

    step = 0
    writer = tf.summary.FileWriter(util.SUMMARIES_DIR, train_sess.graph)
    for epoch in range(hparams.epochs):
        train_sess.run(train_model.iterator.initializer)
        epoch_loss = 0
        train_start = time.time()
        load_time = 0
        train_load_time = 0
        while True:
            try:
                t1 = time.time()
                t2 = time.time()
                # print(train_model.model.debug(train_sess))
                # sys.exit(0)
                step_result = train_model.model.train(train_sess)
                t3 = time.time()
                load_time += t2 - t1
                train_load_time += t3 - t2
                (_, step_loss, step_pred_loss, summary) = step_result
                writer.add_summary(summary, step)
                epoch_loss += step_loss
                step += 1
                if step % hparams.show_step == 0:
                    print('at step {0:d} , total loss: {1:.4f}, logloss: {2:.4f}' \
                          .format(step, step_loss, step_pred_loss))
            except tf.errors.OutOfRangeError:
                print('finish one epoch!')
                break
        train_end = time.time()
        train_time = train_end - train_start
        checkpoint_path = train_model.model.saver.save(
            sess=train_sess,
            save_path=util.MODEL_DIR + 'epoch_' + str(epoch))
        print(checkpoint_path)
        eval_start = time.time()
        train_res = run_eval(eval_model_train, eval_train_sess,
                             checkpoint_path, util.TRAIN_NUM,
                             hparams, flag='train')
        eval_res = run_eval(eval_model_eval, eval_eval_sess,
                            checkpoint_path, util.EVAL_NUM,
                            hparams, flag='eval')
        eval_end = time.time()
        eval_time = eval_end - eval_start
        train_info = ', '.join(
            [str(item[0]) + ':' + str(item[1])
             for item in sorted(train_res.items(), key=lambda x: x[0])])
        eval_info = ', '.join(
            [str(item[0]) + ':' + str(item[1])
             for item in sorted(eval_res.items(), key=lambda x: x[0])])
        # print('at epoch {0:d} , train loss: {1:.4f}, train auc: {2:.4f}, val loss: {3:.4f}, val auc: {4:.4f}'.format(epoch, train_logloss, train_auc, eval_logloss, eval_auc))
        # print('at epoch {0:d} , train time: {1:.1f} eval time: {2:.1f}'.format(epoch, train_time, eval_time))
        # print('at epoch {0:d} , load data time: {1:.1f} train and load time: {2:.1f}'.format(epoch, load_time, train_load_time))
        logger.info('at epoch {0:d}'.format(epoch) + ' train info: ' \
                    + train_info + ' eval info: ' + eval_info)
        logger.info('at epoch {0:d} , train time: {1:.1f} eval time: {2:.1f}' \
                    .format(epoch, train_time, eval_time))
        logger.info('at epoch {0:d} , load data time: {1:.1f} train and load time: {2:.1f}' \
                    .format(epoch, load_time, train_load_time))
        logger.info('\n')
    writer.close()


def test_train():
    # 训练的过程中创建了参数
    check_tensorflow_version()
    check_and_mkdir()
    hparams = create_hparams()
    if hparams.method == 'Train':
        # 将参数保存到model文件夹当中, predict需要将该参数继续载入,来build graph
        save_hparams(util.MODEL_DIR, hparams)
        train(hparams)
    else:
        # 加载train保存的hparams
        hparams_prev = load_hparams(util.MODEL_DIR)
        hparams = extend_hparams(hparams, hparams_prev)
        infer(hparams)

        # test_train()
