import tensorflow as tf

a = tf.constant([[6.0, 6.0, 10.0], [3.0, 4.0, 5.0]])
b = tf.nn.l2_normalize(a, dim=1)

with tf.Session() as sess:
    print(sess.run(b))
