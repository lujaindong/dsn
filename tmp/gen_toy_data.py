# -*- coding: utf-8 -*-
import random

DATA_NUM = 1000
train_file = '../data/train.all.norm.fieldwise.toy.txt'
eval_file = '../data/eval.all.norm.fieldwise.toy.txt'
infer_file = '../data/infer.all.norm.fieldwise.toy.txt'

WORD_INDEX_NUM = 200
WORD_NUM = 5
NEWS_NUM = 3


def gen_toy_data(filename):
    out = open(filename, 'w')
    for _ in range(DATA_NUM):
        lable = str(0) if random.random() < 0.8 else str(1)
        feature_list = []
        feature_list.append(lable)

        # gen candidate news feature
        word_num = WORD_NUM
        news_line = 'CandidateNews:'
        news_word = []
        for _ in range(word_num):
            feat_index = random.randint(1, WORD_INDEX_NUM)
            news_word.append(feat_index)
        news_line += ','.join(map(str, news_word))
        feature_list.append(news_line)

        # generate clicked news feature
        news_num = random.randint(1, NEWS_NUM)
        for news_idx in range(news_num):
            word_num = WORD_NUM
            line = 'clickedNews' + str(news_idx) + ':'
            tmp = []
            for _ in range(word_num):
                feat_index = random.randint(1, WORD_INDEX_NUM)
                tmp.append(feat_index)
            line += ','.join(map(str, tmp))
            feature_list.append(line)

        # generate clicked documents feature
        news_num = random.randint(1, NEWS_NUM)
        for news_idx in range(news_num):
            word_num = WORD_NUM
            line = 'clickedDoc' + str(news_idx) + ':'
            tmp = []
            for _ in range(word_num):
                feat_index = random.randint(1, WORD_INDEX_NUM)
                tmp.append(feat_index)
            line += ','.join(map(str, tmp))
            feature_list.append(line)
        out.write(' '.join(feature_list) + '\n')
    out.close()


gen_toy_data(train_file)
gen_toy_data(eval_file)
gen_toy_data(infer_file)
